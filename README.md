# BCW

Breast Cancer Wisconsin (BCW) — data science project to predict the type of 
cancer from biometric data of epithelial cells. The program's algorithm, which 
is a neural network, was trained on a sample of 699 data sets of patients who 
participated in the same-name study. They are freely available in [open source 
project](https://archive.ics.uci.edu/ml/datasets/Breast%20Cancer%20Wisconsin%20(Diagnostic)).